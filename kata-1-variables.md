# First Kata: Variables and Types

## Practice:

- Create a program that uses the __input()__ function to request the user his or her name. Then the program should be able to print
  the following message: "Hello {typed name here}! Welcome to your first Python program!
  
- Explain which __type__ is associated with the variable __my_variable_one__ at the end of the following code snippet:

  ```python
  my_variable_one: str = "123"
  my_variable_one = int(my_variable_one)
  my_variable_one = my_variable_one + 2324.68
  print(my_variable_one)
  ```

- Explain which is the type associated with the variable __component_one__ at the end of the execution of the following
  code:
  
  ```python
  elements = ['a', 1, 'b', 2, 'c', 3]
  component_one = float(elements[3])
  print(component_one)
  ```
  
 - What type is associated with the variable __x1__ after the execution of the following code (justify your answer):
    
    Note: The following is the for of a cuadratic equation
 
    ```math
    ax^2+bx+c
    
    ```
    
    The following is the fomula used to compute the equation's solutions:
    
    
    ```math
    \Delta = b^2 - 4ac
    
    ```
    ```math
    x_{1} = \frac{-b - \sqrt{\Delta}}{2a}
    
    ```
    ```math
    x_{2} = \frac{-b + \sqrt{\Delta}}{2a}
    
    ```
 
   ```python
   # The following code finds two solutions for a quadratic equation using _axˆ2+bx+c_ 
   a: float = 0
   b: float = 0
   c: float = 0
   
   discriminant: float = (b ** 2) - 4 * a * c
   
   x1 = ((-1 * b) - (discriminant) ** (1 / 2)) / (2 * a)
   x2 = ((-1 * b) - (discriminant) ** (1 / 2)) / (2 * a)
   
   print("solution for x1: {}".format(x1))
   print("solution for x2: {}".format(x2))
   ```
   
  - Based on the following formula to obtain the volume of a cone. Create a Python program that given the variables __r__ for radius and __h__ 
    for height, computes and prints the total volume of a cone. 
  
    ```math
    V = \frac{1}{3} \pi r^{2}h

    ```
    
  - Based on the following formula to obtain the distance between two points. Write a Python program that given variables: __x1__, __y1__, __x2__, __y2__, computes
    the distance between two points:


     ```math
    distance = \sqrt{(x_{2}-x_{1})^2 + (y_{2}-y_{1})^2}

    ```