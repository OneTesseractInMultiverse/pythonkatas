# Python Katas

Python Katas is a repository that contains summarized concepts around object
oriented and functional programming with Python. The Katas cover specific areas
in Python programming and are designed to provide a quick reference, examples and
problems that will allow a person who is currently learning Python, to understand
the concepts and apply them to solve programming problems. 

## Collaborators:

- Pedro Guzmán (pedro@subvertic.com)


## Contents

### White Belt: Rookie

-  [1st Kata: Variables and Types](kata-1-variables.md)
-  [2nd Kata: Control Flow](kata-2-control-flow.md)

### Yellow Belt: 5th Kyu

### Orange Belt: 4th Kyu

### Green Belt: 3th Kyu

### Blue Belt: 2nd Kyu

### Brown Belt: 1st Kyu

### Black Belt: 1st Dan